﻿using System;

namespace Warehouse.UseCases.DTO.Outbound
{
   public class CargoReadDto
   {
      public long Id { get; set; }

      public DateTime CreateDateTime { get; set; }

      public string Status { get; set; }

      public string Consignee { get; set; }
   }
}
