﻿
namespace Warehouse.UseCases.DTO.Outbound
{
   public class CargoSpecificationReadDto
   {
      public long Id { get; set; }

      public long ProductId { get; set; }

      public int Quantity { get; set; }

      public long CargoId { get; set; }
   }
}
