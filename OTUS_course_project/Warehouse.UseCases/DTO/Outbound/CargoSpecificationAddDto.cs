﻿
namespace Warehouse.UseCases.DTO.Outbound
{
   public class CargoSpecificationAddDto
   {
      public long ProductId { get; set; }

      public int Quantity { get; set; }

      public long CargoId { get; set; }
   }
}
