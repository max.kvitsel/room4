﻿using System;

namespace Warehouse.UseCases.DTO.Outbound
{
   public class CargoAddDto
   {
      public DateTime CreateDateTime { get; set; }

      public string Status { get; set; }

      public string Consignee { get; set; }
   }
}
