﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.UseCases.DTO.Storage
{
    internal class RabbitDTO
    {
        public string EntityType { get; set; }
        public Dictionary<string, object> Data { get; set; }

        public ProductAddDto GetProductAddDto ()
        {
            ProductAddDto productAddDto = new();
            productAddDto.Name = Data.TryGetValue("Name", out var name) ? (string)name: null;
            productAddDto.Sku = Data.TryGetValue("Sku", out var sku) ? (string)sku : null;
            productAddDto.Volume = Data.TryGetValue("Volume", out var volume) ? (float)volume : default;

            return productAddDto;
        }
    }
}
