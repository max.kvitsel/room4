﻿
namespace Warehouse.UseCases.DTO.Storage
{
   public class ProductReadDto
   {
      public long Id { get; set; }

      public string Name { get; set; }

      public string Sku { get; set; }

      public float Volume { get; set; }
   }
}
