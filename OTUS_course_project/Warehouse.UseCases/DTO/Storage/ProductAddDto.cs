﻿
namespace Warehouse.UseCases.DTO.Storage
{
   public class ProductAddDto
   {
      public string Name { get; set; }

      public string Sku { get; set; }

      public float Volume { get; set; }
   }
}
