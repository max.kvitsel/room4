﻿
namespace Warehouse.UseCases.DTO.Storage
{
   public class ProductCardReadDto
   {
      public long Id { get; set; }

      public long ProductId { get; set; }

      public int Quantity { get; set; }
   }
}
