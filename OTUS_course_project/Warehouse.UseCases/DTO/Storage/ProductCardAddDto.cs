﻿
namespace Warehouse.UseCases.DTO.Storage
{
   public class ProductCardAddDto
   {
      public long ProductId { get; set; }

      public int Quantity { get; set; }
   }
}
