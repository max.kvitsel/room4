﻿
namespace Warehouse.UseCases.DTO.Inbound
{
   public class OrderSpecificationAddDto
   {
      public long ProductId { get; set; }

      public int Quantity { get; set; }

      public long OrderId { get; set; }
   }
}
