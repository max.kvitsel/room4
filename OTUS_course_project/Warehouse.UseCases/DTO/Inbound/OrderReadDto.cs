﻿using System;

namespace Warehouse.UseCases.DTO.Inbound
{
   public class OrderReadDto
   {
      public long Id { get; set; }

      public DateTime CreateDateTime { get; set; }

      public string Status { get; set; }

      public string Provider { get; set; }
   }
}
