﻿using System;

namespace Warehouse.UseCases.DTO.Inbound
{
   public class OrderAddDto
   {
      public DateTime CreateDateTime { get; set; }

      public string Status { get; set; }

      public string Provider { get; set; }
   }
}
