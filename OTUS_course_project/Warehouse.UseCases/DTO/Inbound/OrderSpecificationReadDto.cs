﻿
namespace Warehouse.UseCases.DTO.Inbound
{
   public class OrderSpecificationReadDto
   {
      public long Id { get; set; }

      public long ProductId { get; set; }

      public int Quantity { get; set; }

      public long OrderId { get; set; }
   }
}
