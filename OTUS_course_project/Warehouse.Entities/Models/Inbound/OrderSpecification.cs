﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Warehouse.Entities.Models.Storage;

namespace Warehouse.Entities.Models.Inbound
{
   [Table("order_specification", Schema = "inbound")]
   public class OrderSpecification
   {
      [Key]
      [Column("id")]
      public long Id { get; set; }

      [Required]
      [Column("id_product")]
      public long ProductId { get; set; }

      [Required]
      [Column("quantity")]
      public int Quantity { get; set; }

      [Required]
      [Column("id_order")]
      public long OrderId { get; set; }

      public Product Product { get; set; }

      public Order Order { get; set; }
   }
}
