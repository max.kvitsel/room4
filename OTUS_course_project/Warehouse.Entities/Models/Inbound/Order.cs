﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Entities.Models.Inbound
{
   [Table("order", Schema = "inbound")]
   public class Order
   {
      [Key]
      [Column("id")]
      public long Id { get; set; }

      [Required]
      [Column("created")]
      public DateTime CreateDateTime { get; set; }

      [Required]
      [Column("status")]
      [MaxLength(255)]
      public string Status { get; set; }

      [Required]
      [Column("provider")]
      [MaxLength(255)]
      public string Provider { get; set; }

      public List<OrderSpecification> OrderSpecification { get; set; }
   }
}
