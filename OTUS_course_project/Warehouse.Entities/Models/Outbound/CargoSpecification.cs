﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Models.Storage;

namespace Warehouse.Entities.Models.Outbound
{
   [Table("cargo_specification", Schema = "outbound")]
   public class CargoSpecification
   {
      [Key]
      [Column("id")]
      public long Id { get; set; }

      [Required]
      [Column("id_product")]
      public long ProductId { get; set; }

      [Required]
      [Column("quantity")]
      public int Quantity { get; set; }

      [Required]
      [Column("id_cargo")]
      public long CargoId { get; set; }

      public Product Product { get; set; }

      public Cargo Cargo { get; set; }
   }
}
