﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Entities.Models.Outbound
{
   [Table("cargo", Schema = "outbound")]
   public class Cargo
   {
      [Key]
      [Column("id")]
      public long Id { get; set; }

      [Required]
      [Column("created")]
      public DateTime CreateDateTime { get; set; }

      [Required]
      [Column("status")]
      [MaxLength(255)]
      public string Status { get; set; }

      [Required]
      [Column("consignee")]
      [MaxLength(255)]
      public string Consignee { get; set; }

      public List<CargoSpecification> CargoSpecification { get; set; }
   }
}
