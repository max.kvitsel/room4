﻿using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Warehouse.Entities.Models.Inbound;
using Warehouse.Entities.Models.Outbound;

namespace Warehouse.Entities.Models.Storage
{
   [Table("product", Schema = "storage")]
   [Index(nameof(Product.Sku), IsUnique = true)]
   public class Product
   {
      [Key]
      [Column("id")]
      public long Id { get; set; }

      [Required]
      [Column("name")]
      [MaxLength(255)]
      public string Name { get; set; }

      [Required]
      [Column("sku")]
      [MaxLength(255)]
      public string Sku { get; set; }

      [Required]
      [Column("volume")]
      public float Volume { get; set; }

      public List<ProductCard> ProductCard { get; set; }

      public List<OrderSpecification> OrderSpecification { get; set; }

      public List<CargoSpecification> CargoSpecification { get; set; }

      public override string ToString()
      {
         return $"Name: {this.Name}; SKU: {this.Sku}; Volume {this.Volume}";
      }
   }
}
