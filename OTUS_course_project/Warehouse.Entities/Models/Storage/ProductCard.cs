﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Entities.Models.Storage
{
   [Table("product_card", Schema = "storage")]
   public class ProductCard
   {
      [Key]
      [Column("id")]
      public long Id { get; set; }

      [Required]
      [Column("id_product")]
      [MaxLength(255)]
      public long ProductId { get; set; }

      [Required]
      [Column("quantity")]
      public int Quantity { get; set; }

      public Product Product { get; set; }
   }
}
