﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Warehouse.Entities.Models.Authentication
{
   [Table("users", Schema = "authentication")]
   public class User
   {
      [Key]
      [Column("id")]
      public long Id { get; set; }

      [Required]
      [Column("username")]
      [MaxLength(255)]
      public string Username { get; set; }

      [Required]
      [Column("password")]
      [MaxLength(255)]
      [JsonIgnore]
      public string Password { get; set; }
   }
}
