﻿using System.ComponentModel.DataAnnotations;

namespace Warehouse.Entities.Models.Authentication
{
   public class AuthenticateRequest
   {
      [Required]
      public string Username { get; set; }

      [Required]
      public string Password { get; set; }
   }
}
