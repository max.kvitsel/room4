﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Warehouse.UseCases.DTO.Storage;


namespace Rabbit.Sender.Controllers.Storage
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IBus _bus;
        public ProductController(IBus bus)
        {
            _bus = bus;
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductAddDto product)
        {
            if (product != null)
            {
                Uri uri = new Uri("rabbitmq://localhost/WarehouseQueue");
                var endPoint = await _bus.GetSendEndpoint(uri);
                await endPoint.Send(product);
                return Ok();
            }
            return BadRequest();
        }
    }
}
