﻿using System;

namespace Shared.Models
{
    public class Product
    {
        public string Name { get; set; }

        public string Sku { get; set; }

        public float Volume { get; set; }
    }
}
