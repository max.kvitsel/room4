﻿using AutoMapper;
using Warehouse.Entities.Models.Inbound;
using Warehouse.UseCases.DTO.Inbound;

namespace Warehouse.API.Profiles.Inbound
{
   public class OrderSpecificationProfile : Profile
   {
      public OrderSpecificationProfile()
      {
         CreateMap<OrderSpecification, OrderSpecificationReadDto>();
         CreateMap<OrderSpecificationAddDto, OrderSpecification>();
      }
   }
}
