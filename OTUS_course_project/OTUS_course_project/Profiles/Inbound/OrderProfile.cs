﻿using AutoMapper;
using Warehouse.Entities.Models.Inbound;
using Warehouse.UseCases.DTO.Inbound;

namespace Warehouse.API.Profiles.Inbound
{
   public class OrderProfile : Profile
   {
      public OrderProfile()
      {
         CreateMap<Order, OrderReadDto>();
         CreateMap<OrderAddDto, Order>();
      }
   }
}
