﻿using AutoMapper;
using Warehouse.Entities.Models.Storage;
using Warehouse.UseCases.DTO.Storage;

namespace Warehouse.API.Profiles.Storage
{
   public class ProductProfile : Profile
   {
      public ProductProfile()
      {
         CreateMap<Product, ProductReadDto>();
         CreateMap<ProductAddDto, Product>();
      }
   }
}
