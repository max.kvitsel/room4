﻿using AutoMapper;
using Warehouse.Entities.Models.Storage;
using Warehouse.UseCases.DTO.Storage;

namespace Warehouse.API.Profiles.Storage
{
   public class ProductCardProfile : Profile
   {
      public ProductCardProfile()
      {
         CreateMap<ProductCard, ProductCardReadDto>();
         CreateMap<ProductCardAddDto, ProductCard>();
      }
   }
}
