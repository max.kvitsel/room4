﻿using AutoMapper;
using Warehouse.Entities.Models.Outbound;
using Warehouse.UseCases.DTO.Outbound;

namespace Warehouse.API.Profiles.Outbound
{
   public class CargoSpecificationProfile : Profile
   {
      public CargoSpecificationProfile()
      {
         CreateMap<CargoSpecification, CargoSpecificationReadDto>();
         CreateMap<CargoSpecificationAddDto, CargoSpecification>();
      }
   }
}
