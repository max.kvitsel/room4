﻿using AutoMapper;
using Warehouse.Entities.Models.Outbound;
using Warehouse.UseCases.DTO.Outbound;

namespace Warehouse.API.Profiles.Outbound
{
   public class CargoProfile : Profile
   {
      public CargoProfile()
      {
         CreateMap<Cargo, CargoReadDto>();
         CreateMap<CargoAddDto, Cargo>();
      }
   }
}
