﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.DataAccess.Postgres.Repository;
using Warehouse.Entities.Models.Inbound;
using Warehouse.UseCases.DTO.Inbound;

namespace Warehouse.API.Controllers.Inbound
{
   [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
   [Route("api/orders")]
   [ApiController]
   public class OrderController : ControllerBase
   {
      private readonly IWarehouseRepository _repository;
      private readonly IMapper _mapper;

      public OrderController(IWarehouseRepository repository, IMapper mapper)
      {
         _repository = repository;
         _mapper = mapper;
      }

      [HttpGet("{id}", Name = "GetOrderById")]
      public async Task<ActionResult<OrderReadDto>> GetOrderById(long id)
      {
         var orderItem = await _repository.GetOrderByIdAsync(id);

         if (orderItem == null)
            return NotFound();

         return Ok(_mapper.Map<OrderReadDto>(orderItem));
      }

      [HttpGet]
      public ActionResult<IEnumerable<OrderReadDto>> GetAllOrders()
      {
         var orderItems = _repository.GetAllOrders();
         return Ok(_mapper.Map<IEnumerable<OrderReadDto>>(orderItems));
      }

      [HttpPost]
      public async Task<ActionResult<OrderReadDto>> AddOrderAsync(OrderAddDto order)
      {
         var orderModel = _mapper.Map<Order>(order);
         _repository.AddOrder(orderModel);
         await _repository.SaveChangesAsync();

         var orderReadDto = _mapper.Map<OrderReadDto>(orderModel);

         return CreatedAtRoute(nameof(GetOrderById), new { Id = orderReadDto.Id }, orderReadDto);
      }
   }
}
