﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.DataAccess.Postgres.Repository;
using Warehouse.Entities.Models.Inbound;
using Warehouse.UseCases.DTO.Inbound;

namespace Warehouse.API.Controllers.Inbound
{
   [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
   [Route("api/orderspecifications")]
   [ApiController]
   public class OrderSpecificationController : ControllerBase
   {
      private readonly IWarehouseRepository _repository;
      private readonly IMapper _mapper;

      public OrderSpecificationController(IWarehouseRepository repository, IMapper mapper)
      {
         _repository = repository;
         _mapper = mapper;
      }

      [HttpGet("{id}", Name = "GetOrderSpecificationById")]
      public async Task<ActionResult<OrderSpecificationReadDto>> GetOrderSpecificationById(long id)
      {
         var orderSpecificationItem = await _repository.GetOrderSpecificationByIdAsync(id);

         if (orderSpecificationItem == null)
            return NotFound();

         return Ok(_mapper.Map<OrderSpecificationReadDto>(orderSpecificationItem));
      }

      [HttpGet]
      public ActionResult<IEnumerable<OrderSpecificationReadDto>> GetAllOrderSpecifications()
      {
         var orderSpecificationItems = _repository.GetAllOrderSpecifications();
         return Ok(_mapper.Map<IEnumerable<OrderSpecificationReadDto>>(orderSpecificationItems));
      }

      [HttpPost]
      public async Task<ActionResult<OrderSpecificationReadDto>> AddOrderSpecificationAsync(OrderSpecificationAddDto order)
      {
         var orderSpecificationModel = _mapper.Map<OrderSpecification>(order);
         _repository.AddOrderSpecification(orderSpecificationModel);
         await _repository.SaveChangesAsync();

         var orderSpecificationReadDto = _mapper.Map<OrderSpecificationReadDto>(orderSpecificationModel);

         return CreatedAtRoute(nameof(GetOrderSpecificationById), new { Id = orderSpecificationReadDto.Id }, orderSpecificationReadDto);
      }
   }
}
