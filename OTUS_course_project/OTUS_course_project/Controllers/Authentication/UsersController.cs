﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Warehouse.DataAccess.Postgres.Repository;
using Warehouse.Entities.Models.Authentication;

namespace Warehouse.API.Controllers.Authentication
{
   [Route("api/users")]
   [ApiController]
   public class UsersController : ControllerBase
   {
      private readonly IWarehouseRepository _repository;
      private readonly IConfiguration _configuration;

      public UsersController(IWarehouseRepository repository, IConfiguration configuration)
      {
         _repository = repository;
         _configuration = configuration;
      }

      [HttpPost("authenticate")]
      public async Task<IActionResult> Authenticate(AuthenticateRequest model)
      {
         var user = await _repository.Authenticate(model);

         if (user == null)
            return BadRequest(new { message = "Username or password is incorrect" });

         var tokenHandler = new JwtSecurityTokenHandler();
         var key = Encoding.ASCII.GetBytes(_configuration["AppSettings:Secret"]);
         var tokenDescriptor = new SecurityTokenDescriptor
         {
            Subject = new ClaimsIdentity(new Claim[]
               {
                     new Claim(ClaimTypes.Name, user.Id.ToString())
               }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
         };
         var token = tokenHandler.CreateToken(tokenDescriptor);
         var tokenString = tokenHandler.WriteToken(token);

         // return basic user info and authentication token
         return Ok(new
         {
            Id = user.Id,
            Username = user.Username,
            Token = tokenString
         });
      }

      [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
      [HttpGet]
      public IActionResult GetAll()
      {
         var users = _repository.GetAllUsers();
         return Ok(users);
      }
   }
}
