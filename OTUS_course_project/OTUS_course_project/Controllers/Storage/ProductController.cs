﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.DataAccess.Postgres.Repository;
using Warehouse.Entities.Models.Storage;
using Warehouse.UseCases.DTO.Storage;

namespace Warehouse.API.Controllers.Storage
{
   [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
   [Route("api/products")]
   [ApiController]
   public class ProductController : ControllerBase
   {
      private readonly IWarehouseRepository _repository;
      private readonly IMapper _mapper;
      
      public ProductController(IWarehouseRepository repository, IMapper mapper)
      {
         _repository = repository;
         _mapper = mapper;
      }
      
      [HttpGet("{id}", Name = "GetProductById")]
      public async Task<ActionResult<ProductReadDto>> GetProductById(long id)
      {
         var productItem = await _repository.GetProductByIdAsync(id);
      
         if (productItem == null)
            return NotFound();
      
         return Ok(_mapper.Map<ProductReadDto>(productItem));
      }
      
      [HttpGet]
      public ActionResult<IEnumerable<ProductReadDto>> GetAllProducts()
      {
         var productItems = _repository.GetAllProducts();
         return Ok(_mapper.Map<IEnumerable<ProductReadDto>>(productItems));
      }
      
      [HttpPost]
      public async Task<ActionResult<ProductReadDto>> AddProductAsync(ProductAddDto product)
      {
         var productModel = _mapper.Map<Product>(product);
         _repository.AddProduct(productModel);
         await _repository.SaveChangesAsync();
      
         var productReadDto = _mapper.Map<ProductReadDto>(productModel);
      
         return CreatedAtRoute(nameof(GetProductById), new { Id = productReadDto.Id }, productReadDto);
      }
   }
}
