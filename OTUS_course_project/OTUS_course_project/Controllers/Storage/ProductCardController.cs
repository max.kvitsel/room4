﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.DataAccess.Postgres.Repository;
using Warehouse.Entities.Models.Storage;
using Warehouse.UseCases.DTO.Storage;

namespace Warehouse.API.Controllers.Storage
{
   [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
   [Route("api/productcards")]
   [ApiController]
   public class ProductCardController : ControllerBase
   {
      private readonly IWarehouseRepository _repository;
      private readonly IMapper _mapper;

      public ProductCardController(IWarehouseRepository repository, IMapper mapper)
      {
         _repository = repository;
         _mapper = mapper;
      }

      [HttpGet("{id}", Name = "GetProductCardById")]
      public async Task<ActionResult<ProductCardReadDto>> GetProductCardById(long id)
      {
         var productCardItem = await _repository.GetProductCardByIdAsync(id);

         if (productCardItem == null)
            return NotFound();

         return Ok(_mapper.Map<ProductCardReadDto>(productCardItem));
      }

      [HttpGet]
      public ActionResult<IEnumerable<ProductCardReadDto>> GetAllProductCards()
      {
         var productCardItem = _repository.GetAllProductCards();
         return Ok(_mapper.Map<IEnumerable<ProductCardReadDto>>(productCardItem));
      }

      [HttpPost]
      public async Task<ActionResult<ProductCardReadDto>> AddProductCardAsync(ProductCardAddDto productCard)
      {
         var productCardModel = _mapper.Map<Product>(productCard);
         _repository.AddProduct(productCardModel);
         await _repository.SaveChangesAsync();

         var productCardReadDto = _mapper.Map<ProductCardReadDto>(productCardModel);

         return CreatedAtRoute(nameof(GetProductCardById), new { Id = productCardReadDto.Id }, productCardReadDto);
      }
   }
}
