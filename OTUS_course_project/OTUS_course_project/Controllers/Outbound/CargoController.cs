﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.DataAccess.Postgres.Repository;
using Warehouse.Entities.Models.Outbound;
using Warehouse.UseCases.DTO.Outbound;

namespace Warehouse.API.Controllers.Outbound
{
   [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
   [Route("api/cargos")]
   [ApiController]
   public class CargoController : ControllerBase
   {
      private readonly IWarehouseRepository _repository;
      private readonly IMapper _mapper;

      public CargoController(IWarehouseRepository repository, IMapper mapper)
      {
         _repository = repository;
         _mapper = mapper;
      }

      [HttpGet("{id}", Name = "GetCargoById")]
      public async Task<ActionResult<CargoReadDto>> GetCargoById(long id)
      {
         var cargoItem = await _repository.GetCargoByIdAsync(id);

         if (cargoItem == null)
            return NotFound();

         return Ok(_mapper.Map<CargoReadDto>(cargoItem));
      }

      [HttpGet]
      public ActionResult<IEnumerable<CargoReadDto>> GetAllCargos()
      {
         var cargoItems = _repository.GetAllCargos();
         return Ok(_mapper.Map<IEnumerable<CargoReadDto>>(cargoItems));
      }

      [HttpPost]
      public async Task<ActionResult<CargoReadDto>> AddCargoAsync(CargoAddDto cargo)
      {
         var cargoModel = _mapper.Map<Cargo>(cargo);
         _repository.AddCargo(cargoModel);
         await _repository.SaveChangesAsync();

         var cargoReadDto = _mapper.Map<CargoReadDto>(cargoModel);

         return CreatedAtRoute(nameof(GetCargoById), new { Id = cargoReadDto.Id }, cargoReadDto);
      }
   }
}
