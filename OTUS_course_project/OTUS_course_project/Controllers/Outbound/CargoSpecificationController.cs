﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.DataAccess.Postgres.Repository;
using Warehouse.Entities.Models.Outbound;
using Warehouse.UseCases.DTO.Outbound;

namespace Warehouse.API.Controllers.Outbound
{
   [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
   [Route("api/cargospecifications")]
   [ApiController]
   public class CargoSpecificationController : ControllerBase
   {
      private readonly IWarehouseRepository _repository;
      private readonly IMapper _mapper;

      public CargoSpecificationController(IWarehouseRepository repository, IMapper mapper)
      {
         _repository = repository;
         _mapper = mapper;
      }

      [HttpGet("{id}", Name = "GetCargoSpecificationById")]
      public async Task<ActionResult<CargoSpecificationReadDto>> GetCargoSpecificationById(long id)
      {
         var cargoSpecificationItem = await _repository.GetCargoSpecificationByIdAsync(id);

         if (cargoSpecificationItem == null)
            return NotFound();

         return Ok(_mapper.Map<CargoSpecificationReadDto>(cargoSpecificationItem));
      }

      [HttpGet]
      public ActionResult<IEnumerable<CargoSpecificationReadDto>> GetAllCargoSpecifications()
      {
         var cargoSpecificationItems = _repository.GetAllCargoSpecifications();
         return Ok(_mapper.Map<IEnumerable<CargoSpecificationReadDto>>(cargoSpecificationItems));
      }

      [HttpPost]
      public async Task<ActionResult<CargoSpecificationReadDto>> AddCargoSpecificationAsync(CargoSpecificationAddDto cargo)
      {
         var cargoSpecificationModel = _mapper.Map<CargoSpecification>(cargo);
         _repository.AddCargoSpecification(cargoSpecificationModel);
         await _repository.SaveChangesAsync();

         var cargoSpecificationReadDto = _mapper.Map<CargoSpecificationReadDto>(cargoSpecificationModel);

         return CreatedAtRoute(nameof(GetCargoSpecificationById), new { Id = cargoSpecificationReadDto.Id }, cargoSpecificationReadDto);
      }
   }
}
