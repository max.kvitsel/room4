﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Warehouse.Entities.Models.Authentication;
using Warehouse.Entities.Models.Inbound;
using Warehouse.Entities.Models.Outbound;
using Warehouse.Entities.Models.Storage;

namespace Warehouse.DataAccess.Postgres.Repository
{
   public interface IWarehouseRepository
   {
      IEnumerable<Product> GetAllProducts();

      Task<Product> GetProductByIdAsync(long id);

      void AddProduct(Product product);

      IEnumerable<ProductCard> GetAllProductCards();

      Task<ProductCard> GetProductCardByIdAsync(long id);

      void AddProductCard(ProductCard productCard);

      IEnumerable<Order> GetAllOrders();

      Task<Order> GetOrderByIdAsync(long id);

      void AddOrder(Order order);

      IEnumerable<OrderSpecification> GetAllOrderSpecifications();

      Task<OrderSpecification> GetOrderSpecificationByIdAsync(long id);

      void AddOrderSpecification(OrderSpecification orderSpecification);

      IEnumerable<Cargo> GetAllCargos();

      Task<Cargo> GetCargoByIdAsync(long id);

      void AddCargo(Cargo cargo);

      IEnumerable<CargoSpecification> GetAllCargoSpecifications();

      Task<CargoSpecification> GetCargoSpecificationByIdAsync(long id);

      void AddCargoSpecification(CargoSpecification cargoSpecification);

      Task<User> Authenticate(AuthenticateRequest model);

      IEnumerable<User> GetAllUsers();

      Task<User> GetUserByIdAsync(long id);

      Task SaveChangesAsync();
   }
}
