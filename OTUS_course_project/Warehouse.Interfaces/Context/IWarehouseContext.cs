﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Warehouse.Entities.Models.Authentication;
using Warehouse.Entities.Models.Inbound;
using Warehouse.Entities.Models.Outbound;
using Warehouse.Entities.Models.Storage;

namespace Warehouse.Interfaces.Context
{
   public interface IWarehouseContext
   {
      DbSet<Product> Products { get; set; }
      DbSet<ProductCard> ProductCards { get; set; }
      DbSet<Order> Orders { get; set; }
      DbSet<OrderSpecification> OrderSpecifications { get; set; }
      DbSet<Cargo> Cargos { get; set; }
      DbSet<CargoSpecification> CargoSpecifications { get; set; }
      DbSet<User> Users { get; set; }

      Task SaveChangesAsync();
   }
}
