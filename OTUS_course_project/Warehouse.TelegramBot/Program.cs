﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Telegram.Bot.Types;
using Warehouse.DataAccess.Postgres;
using Warehouse.DataAccess.Postgres.Repository;
    
namespace Warehouse.TelegramBot
{
    public class Program
    {
        static ITelegramBotClient bot = new TelegramBotClient("5287389023:AAEeRLn-BicI6r3zMllQ6ywuUmn2d3iv9ek");
        public static void Config(IConfiguration configuration)
        {
            Configuration = configuration;
            DbContextOptionsBuilder<WarehouseContext> builder = new DbContextOptionsBuilder<WarehouseContext>();
            builder.UseNpgsql(Configuration.GetConnectionString("WarehouseConnection"));
            repository = new WarehouseRepository(new WarehouseContext(builder.Options));
        }
        public static IConfiguration Configuration { get; set; }
        private static WarehouseRepository repository;
        public static async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception,
            CancellationToken cancellationToken)
        {
            // Некоторые действия
            Console.WriteLine(JsonSerializer.Serialize(exception));
        }

        public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
            CancellationToken cancellationToken)
        {
            // Некоторые действия
            Console.WriteLine(JsonSerializer.Serialize(update));
            if (update.Type == Telegram.Bot.Types.Enums.UpdateType.Message)
            {
                string answer = await GetAnswer(update);
                await botClient.SendTextMessageAsync(update.Message.Chat, answer);
            }
        }

        private async static Task<string> GetAnswer(Update update)
        {
            switch (update?.Message?.Text.ToLower())
            {
                case "/start":
                    return "Добро пожаловать, " + update?.Message?.From?.Username;
                case "get products":
                {
                    var res = repository.GetAllProducts();
                    //var res = await repository.GetProductByIdAsync(2);

                    return string.Join("\r\n ,",res);
                }
                default:
                    return "Я не знаю такой команды";
            }
        }


        async static Task Main(string[] args)
        {
            Console.WriteLine("Запущен бот " + bot.GetMeAsync().Result.FirstName);
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var configuration = builder.Build();
            Config(configuration);
           
            var cts = new CancellationTokenSource();
            var cancellationToken = cts.Token;
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }, // receive all update types
            };
            bot.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken
            );

            Console.ReadLine();
        }
    }
}