﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Models.Storage;
using Warehouse.UseCases.DTO.Storage;
using Warehouse.DataAccess.Postgres.Repository;
using AutoMapper;

namespace Rabbit.Consumer.Consumers
{
    public class ProductConsumer : IConsumer<ProductAddDto>, IConsumer<ProductCardAddDto>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly IMapper _mapper;

        public ProductConsumer(IWarehouseRepository warehouseRepository, IMapper mapper)
        {
            _warehouseRepository = warehouseRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<ProductAddDto> context)
        {

            ProductAddDto message = context.Message;

            ProductAddDto item = new ProductAddDto
            {
                Name = message.Name,
                Sku = message.Sku,
                Volume = message.Volume
            };

            var productModel = _mapper.Map<Product>(item);
            _warehouseRepository.AddProduct(productModel);
            await _warehouseRepository.SaveChangesAsync();

        }

        public async Task Consume(ConsumeContext<ProductCardAddDto> context)
        {
            ProductCardAddDto message = context.Message;

            ProductCardAddDto item = new ProductCardAddDto
            {
                ProductId = message.ProductId,
                Quantity = message.Quantity
            };

            var productCardModel = _mapper.Map<ProductCard>(item);
            _warehouseRepository.AddProductCard(productCardModel);
            await _warehouseRepository.SaveChangesAsync();
        }
    }
}
