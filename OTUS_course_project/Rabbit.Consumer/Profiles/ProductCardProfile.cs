﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Models.Storage;
using Warehouse.UseCases.DTO.Storage;

namespace Rabbit.Consumer.Profiles
{
    public class ProductCardProfile : Profile
    {
        public ProductCardProfile()
        {
            CreateMap<ProductCard, ProductCardReadDto>();
            CreateMap<ProductCardAddDto, ProductCard>();
        }
    }
}
