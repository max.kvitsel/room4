﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Entities.Models.Authentication;
using Warehouse.Entities.Models.Inbound;
using Warehouse.Entities.Models.Outbound;
using Warehouse.Entities.Models.Storage;
using Warehouse.Interfaces.Context;

namespace Warehouse.DataAccess.Postgres.Repository
{
   public class WarehouseRepository : IWarehouseRepository
   {
      private readonly IWarehouseContext _context;
      private readonly IConfiguration _configuration;

      public WarehouseRepository(IWarehouseContext context, IConfiguration configuration)
      {
         _context = context;
         _configuration = configuration;
      }
      public void AddProduct(Product product)
      {
         if (product == null)
            throw new ArgumentNullException((nameof(product)));

         _context.Products.Add(product);
      }

      public IEnumerable<Product> GetAllProducts()
      {
         return _context.Products;
      }

      public async Task<Product> GetProductByIdAsync(long id)
      {
         return await _context.Products.FindAsync(id);
      }

      public async Task SaveChangesAsync()
      {
         await _context.SaveChangesAsync();
      }

      public void AddProductCard(ProductCard productCard)
      {
         if (productCard == null)
            throw new ArgumentNullException((nameof(productCard)));

         _context.ProductCards.Add(productCard);
      }

      public IEnumerable<ProductCard> GetAllProductCards()
      {
         return _context.ProductCards;
      }

      public async Task<ProductCard> GetProductCardByIdAsync(long id)
      {
         return await _context.ProductCards.FindAsync(id);
      }

      public void AddOrder(Order order)
      {
         if (order == null)
            throw new ArgumentNullException((nameof(order)));

         _context.Orders.Add(order);
      }

      public IEnumerable<Order> GetAllOrders()
      {
         return _context.Orders;
      }

      public async Task<Order> GetOrderByIdAsync(long id)
      {
         return await _context.Orders.FindAsync(id);
      }

      public void AddOrderSpecification(OrderSpecification orderSpecification)
      {
         if (orderSpecification == null)
            throw new ArgumentNullException((nameof(orderSpecification)));

         _context.OrderSpecifications.Add(orderSpecification);
      }

      public IEnumerable<OrderSpecification> GetAllOrderSpecifications()
      {
         return _context.OrderSpecifications;
      }

      public async Task<OrderSpecification> GetOrderSpecificationByIdAsync(long id)
      {
         return await _context.OrderSpecifications.FindAsync(id);
      }

      public void AddCargo(Cargo cargo)
      {
         if (cargo == null)
            throw new ArgumentNullException((nameof(cargo)));

         _context.Cargos.Add(cargo);
      }

      public IEnumerable<Cargo> GetAllCargos()
      {
         return _context.Cargos;
      }

      public async Task<Cargo> GetCargoByIdAsync(long id)
      {
         return await _context.Cargos.FindAsync(id);
      }

      public void AddCargoSpecification(CargoSpecification cargoSpecification)
      {
         if (cargoSpecification == null)
            throw new ArgumentNullException((nameof(cargoSpecification)));

         _context.CargoSpecifications.Add(cargoSpecification);
      }

      public IEnumerable<CargoSpecification> GetAllCargoSpecifications()
      {
         return _context.CargoSpecifications;
      }

      public async Task<CargoSpecification> GetCargoSpecificationByIdAsync(long id)
      {
         return await _context.CargoSpecifications.FindAsync(id);
      }

      public async Task<User> Authenticate(AuthenticateRequest model)
      {
         var user = await _context.Users
                        .Where(x => x.Username == model.Username && x.Password == model.Password)
                        .FirstOrDefaultAsync();

         if (user == null)
            return null;

         return user;
      }

      public IEnumerable<User> GetAllUsers()
      {
         return _context.Users;
      }

      public async Task<User> GetUserByIdAsync(long id)
      {
         return await _context.Users.FindAsync(id);
      }
   }
}
