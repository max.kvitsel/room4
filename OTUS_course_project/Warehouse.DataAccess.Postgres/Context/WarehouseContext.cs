﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Warehouse.Entities.Models.Authentication;
using Warehouse.Entities.Models.Inbound;
using Warehouse.Entities.Models.Outbound;
using Warehouse.Entities.Models.Storage;
using Warehouse.Interfaces.Context;

namespace Warehouse.DataAccess.Postgres
{
   public class WarehouseContext : DbContext, IWarehouseContext
   {
      public WarehouseContext(DbContextOptions<WarehouseContext> opt) : base(opt) { }

      public DbSet<Product> Products { get; set; }
      public DbSet<ProductCard> ProductCards { get; set; }
      public DbSet<Order> Orders { get; set; }
      public DbSet<OrderSpecification> OrderSpecifications { get; set; }
      public DbSet<Cargo> Cargos { get; set; }
      public DbSet<CargoSpecification> CargoSpecifications { get; set; }
      public DbSet<User> Users { get; set; }

      public async Task SaveChangesAsync()
      {
         await base.SaveChangesAsync();
      }
   }
}
